#!/bin/bash
while true
do
	temp=$(cat /sys/class/thermal/thermal_zone2/temp)
	limit=55000
	if [ $temp -gt $limit ]
	then
		notify-send 'Warning' 'HIGH TEMP!\nCHECK SYSTEM' --icon=/usr/share/icons/Adwaita/32x32/emotes/face-sick-symbolic.symbolic.png -t 15000
	fi
	sleep 60
done
