#!/bin/bash
#$HOME/.dwm
while true
do
	battery=$(cat /sys/class/power_supply/BAT1/capacity)
	date=$(date "+%R %-m/%-d %a ")
	#echo "$battery% $date"
	xsetroot -name " $battery% $date"
	sleep 30
done
