#!/bin/bash
while true
do
	battery=$(cat /sys/class/power_supply/BAT1/capacity)
	battery_status=$(cat /sys/class/power_supply/BAT1/status)
	limit=20
	limit_status='Discharging'
	if [ $battery -lt $limit -a "$battery_status" = "$limit_status" ]
	then
		notify-send 'Warning' 'POWER LOW!\nNEED AC' --icon=/usr/share/icons/Adwaita/32x32/devices/ac-adapter-symbolic.symbolic.png -t 15000
	fi
	sleep 300
done
